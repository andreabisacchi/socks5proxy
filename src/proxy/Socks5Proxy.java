package proxy;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.concurrent.atomic.AtomicBoolean;

import connect.blacklist.Blacklist;
import requests.ClientRequest;
import requests.authentication.ClientAuthenticationRequest;
import requests.authentication.VersionNotSupportedException;

public class Socks5Proxy implements Runnable {
	private static final int DEFAULT_QUEUE_SIZE = 10;
	private static final int DEFAULT_PORT = 1080;
	
	private final int port;
	private final int queueSize;
	private final AtomicBoolean stop = new AtomicBoolean(false);
	
	private final Blacklist blacklist = new Blacklist();
	private Thread thread;
	
	public Socks5Proxy() {
		this(DEFAULT_PORT, DEFAULT_QUEUE_SIZE);
	}
	
	public Socks5Proxy(int port) {
		this(port, DEFAULT_QUEUE_SIZE);
	}
	
	public Socks5Proxy(int port, int queueSize) {
		this.port = port;
		this.queueSize = queueSize;
	}
	
	public int getPort() {
		return this.port;
	}
	
	public Blacklist getBlacklist() {
		return this.blacklist;
	}
	
	@Override
	public void run() {
		ServerSocket socket = null;
		try {
			socket = new ServerSocket(this.port, this.queueSize);
			socket.setSoTimeout(1000);
		} catch (IOException e) {
			System.err.println("Error on opening ServerSocket. " + e.getMessage());
			System.exit(1);
		}
		
		Socket s = null;
		Thread.currentThread().setName(this.toString() + " RUNNING");
		this.stop.set(false);
		while(this.isRunning()) {
			try {
				s = socket.accept();
				final InputStream in = s.getInputStream();
				final OutputStream out = s.getOutputStream();

				final ClientAuthenticationRequest authReq = ClientAuthenticationRequest.parse(in);
				authReq.writeResponse(out);

				final ClientRequest clientReq = ClientRequest.parse(s);
				clientReq.executeAndWriteResponse(out, this);
				
			} catch (SocketTimeoutException e) {
				continue;
			}
			catch(SocketException | UnknownHostException | VersionNotSupportedException e) {
				try { s.close(); } catch (Throwable e1) { }
			} catch(Exception e) {
			}
		}
		Thread.currentThread().setName(this.toString() + " STOPPED");
	}
	
	public String toString() {
		return "Socks5 proxy listening on " + this.port;
	}
	
	public void start() {
		thread = new Thread(this, this.toString() + " IDLE");
		thread.start();
	}
	
	public void stop() {
		this.stop.set(true);
		thread.interrupt();
	}

	public boolean isRunning() {
		return !this.stop.get();
	}

}
