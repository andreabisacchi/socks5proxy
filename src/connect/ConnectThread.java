package connect;

import java.net.InetAddress;
import java.net.Socket;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;

import connect.methods.ConnectMethod;
import proxy.Socks5Proxy;
import requests.ClientRequest;
import requests.addresses.AddressTypes;
import requests.addresses.RequestDomainAddress;
import requests.addresses.RequestIPv4Address;

public class ConnectThread extends Thread {
	private char reply;
	private int port;
	private InetAddress address;

	private ClientRequest clientRequest;
	private Socket clientSocket;

	private AtomicBoolean ready = new AtomicBoolean(false);
	private Semaphore sem = new Semaphore(0);
	
	private Socks5Proxy proxy;

	public ConnectThread(ClientRequest clientRequest, Socket clientSocket, Socks5Proxy proxy) {
		super("Connection thread: " + clientRequest);
		this.clientRequest = clientRequest;
		this.clientSocket = clientSocket;
		this.proxy = proxy;
	}

	public char getReply() {
		this.waitForResponse();
		return this.reply;
	}

	public AddressTypes getAddressType() {
		this.waitForResponse();
		return AddressTypes.IPV4;
	}

	public int getPort() {
		this.waitForResponse();
		return this.port;
	}

	public InetAddress getAddress() {
		this.waitForResponse();
		return this.address;
	}

	@Override
	public void run() {
		try {
			final InetAddress address;
			final int port = this.clientRequest.getPort();

			switch (this.clientRequest.getAddressType()) {
			case DOMAINNAME:
				address = InetAddress.getByName(((RequestDomainAddress)clientRequest.getRequestAddress()).getAddress());
				break;
				
			case IPV4:
				address = ((RequestIPv4Address)clientRequest.getRequestAddress()).getAddress();
				break;
				
			case IPV6:
				throw new IllegalStateException();

			default:
				address = null;
				break;
			}

			try {
				this.proxy.getBlacklist().check(address, port);
				
				ConnectMethod connectMethod = ConnectMethod.getConnectMethod(clientSocket, this.proxy);
				connectMethod.connect(address, port);
				
				this.reply = 0;
				this.port = connectMethod.getLocalPort();
				this.address = connectMethod.getInetAddress();
				this.ready.set(true);
				this.sem.release();
				this.clientRequest.waitForSending();
				
				connectMethod.startConnectionReceiver();
				
			} catch (SecurityException e) {
				this.reply = 2;
				this.port = 0;
				this.address = InetAddress.getLocalHost();
				this.ready.set(true);
				this.sem.release();
				System.err.println("Closed because of blacklist");
			} catch (Exception e) {
				this.reply = 1;
				this.port = 0;
				this.address = InetAddress.getLocalHost();
				this.ready.set(true);
				this.sem.release();
			}
		} catch (Exception i) {}
	}
	
	private void waitForResponse() {
		while (!this.ready.get()) {
			try { this.sem.acquire(); } catch (Exception e) {}
		}
	}

}
