package connect.blacklist;

import java.net.InetAddress;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.function.Predicate;

import connect.poolThread.Pair;

public class Blacklist {
	private static final SecurityException exception = new SecurityException("Not allowed, blacklisted");
	
	private ConcurrentLinkedDeque<Predicate<Pair<InetAddress, Integer>>> blacklistPredicates = new ConcurrentLinkedDeque<>();

	public void check(InetAddress address, int port) throws SecurityException {
		for (Predicate<Pair<InetAddress, Integer>> predicate : blacklistPredicates) {
			if (!predicate.test(new Pair<>(address, port)))
				throw exception;
		}
	}
	
	public void addPredicate(Predicate<Pair<InetAddress, Integer>> predicate) {
		this.blacklistPredicates.add(predicate);
	}
	
	public boolean removePredicate(Predicate<Pair<InetAddress, Integer>> predicate) {
		return this.blacklistPredicates.remove(predicate);
	}
	
	public static Predicate<Pair<InetAddress, Integer>> getPredicatePrivateNetworks() {
		return pair ->  {
			final InetAddress address = pair.getElement0();
			final String hostAddress = address.getHostAddress();
			return  (!
					   hostAddress.startsWith("10") 
					|| hostAddress.startsWith("192.168")
					|| hostAddress.startsWith("172") && (
						   hostAddress.startsWith("172.16") || hostAddress.startsWith("172.17")
						|| hostAddress.startsWith("172.18") || hostAddress.startsWith("172.19")
						|| hostAddress.startsWith("172.20") || hostAddress.startsWith("172.21")
						|| hostAddress.startsWith("172.22") || hostAddress.startsWith("172.23")
						|| hostAddress.startsWith("172.24") || hostAddress.startsWith("172.25")
						|| hostAddress.startsWith("172.26") || hostAddress.startsWith("172.27")
						|| hostAddress.startsWith("172.28") || hostAddress.startsWith("172.29")
						|| hostAddress.startsWith("172.30") || hostAddress.startsWith("172.31")
					)
					);
		};
	}
	
}
