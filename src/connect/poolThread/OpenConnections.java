package connect.poolThread;

import java.io.IOException;
import java.net.Socket;
import java.util.Collection;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentLinkedDeque;

public class OpenConnections {
	private final ConcurrentLinkedDeque<Pair<Socket, Socket>> openConnections = new ConcurrentLinkedDeque<>();
	
	private static OpenConnections instance = new OpenConnections();
	
	public static OpenConnections getInstance() {
		return instance;
	}
	
	protected void addPair(Pair<Socket, Socket> pair) {
		this.openConnections.add(pair);
	}
	
	public void addSockets(Socket socket1, Socket socket2) {
		this.addPair(new Pair<Socket, Socket>(socket1, socket2));
		this.addPair(new Pair<Socket, Socket>(socket2, socket1));
	}
	
	public boolean removeSocket(Socket socket) {
		LinkedList<Pair<Socket, Socket>> toBeRemoved = new LinkedList<>();
		for (Pair<Socket, Socket> pair : openConnections) {
			if (pair.getElement0().equals(socket) || pair.getElement1().equals(socket)) {
				toBeRemoved.add(pair);
			}
		}
		if (toBeRemoved.size() == 0) {
			return false;
		} else {
			for (Pair<Socket, Socket> pair : toBeRemoved) {
				try { pair.getElement0().close(); } catch (IOException e) {	}
				try { pair.getElement1().close(); } catch (IOException e) {	}
				this.openConnections.remove(pair);
			}
			return true;
		}
	}
	
	public Collection<Pair<Socket, Socket>> getCollection() {
		return this.openConnections;
	}
	
	@Override
	public String toString() {
		return this.openConnections.toString();
	}
	
}

