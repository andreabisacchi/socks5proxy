package connect.poolThread;

import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PoolThread extends Thread {
	private static final int ARRAYSIZE = 1024;
	private static final PoolThread instance = new PoolThread();
	
	private final OpenConnections openConnections = OpenConnections.getInstance();
	private final ExecutorService executor = Executors.newWorkStealingPool();

	public PoolThread() {
		super("PoolThread");
		this.setDaemon(true);
	}
	
	@Override
	public void run() {
		for(;;) {
			for (Pair<Socket, Socket> pair : this.openConnections.getCollection()) {
				this.executor.submit(new ConnectionRelay(pair));
			}
			try { Thread.sleep(1);	} catch (InterruptedException e) { }
		}
	}
	
	public void execute() {
		if (!this.isAlive())
			this.start();
	}
	
	public static PoolThread getInstance() {
		return instance;
	}

	private class ConnectionRelay implements Runnable {
		private final OpenConnections openConnections = OpenConnections.getInstance();
		private Pair<Socket, Socket> pair;

		public ConnectionRelay(Pair<Socket, Socket> pair) {
			this.pair = pair;
		}

		@Override
		public void run() {
			int read;
			try {
				if (pair.getElement0().getInputStream().available() > 0) {
					byte[] bytes = new byte[ARRAYSIZE];
					read = pair.getElement0().getInputStream().read(bytes);
					pair.getElement1().getOutputStream().write(bytes, 0, read);
				}
			} catch (IOException e) {
				this.openConnections.removeSocket(pair.getElement0());
				this.openConnections.removeSocket(pair.getElement1());
			}
		}

	}
}