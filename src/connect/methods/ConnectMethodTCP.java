package connect.methods;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

import connect.tcp.ConnectionReceiverTCP;
import proxy.Socks5Proxy;

public class ConnectMethodTCP extends ConnectMethod {
	private static final boolean ENABLETIMEOUT = false;
	private static final int TIMEOUTSOCKET = 1000 * 60 * 5;
	
	private Socket out;

	public ConnectMethodTCP(Socket clientSocket, Socks5Proxy proxy) {
		super(clientSocket, proxy);
	}	
	
	@Override
	public int getLocalPort() {
		return this.out.getLocalPort();
	}

	@Override
	public InetAddress getInetAddress() {
		return this.out.getLocalAddress();
	}

	@Override
	public void connect(InetAddress address, int port) throws IOException {
		this.out = new Socket(address, port);
		if (ENABLETIMEOUT)
			this.out.setSoTimeout(TIMEOUTSOCKET);
		this.out.setKeepAlive(true);
	}
	
	@Override
	public void startConnectionReceiver() {
		//PoolThread.getInstance().execute();
		//OpenConnections.getInstance().addSockets(this.getClientSocket(), out);
		ConnectionReceiverTCP.start(this.getClientSocket(), this.out, this.getProxy());
	}

	@Override
	public final ConnectMethods getMethod() {
		return ConnectMethods.TCP;
	}

}
