package connect.methods;

import java.net.InetAddress;
import java.net.Socket;

import proxy.Socks5Proxy;

public abstract class ConnectMethod {
	private Socket clientSocket;
	
	private final Socks5Proxy proxy;
	
	public ConnectMethod(Socket clientSocket, Socks5Proxy proxy) {
		this.clientSocket = clientSocket;
		this.proxy = proxy;
	}
	
	protected final Socks5Proxy getProxy() {
		return this.proxy;
	}
	
	protected final Socket getClientSocket() {
		return this.clientSocket;
	}
	
	public abstract int getLocalPort();
	
	public abstract InetAddress getInetAddress();
	
	public abstract void connect(InetAddress host, int port) throws Exception;
	
	public abstract ConnectMethods getMethod();
	
	public abstract void startConnectionReceiver();
	
	
	/***** STATIC  *****/
	
	private static ConnectMethods currentMethod = ConnectMethods.TCP;
	
	/**
	 * Changes the defaul connectMethod used.
	 * @param method the default connect method. If null it does nothing
	 */
	public static final void changeConnectMethod(ConnectMethods method) {
		if (method == null)
			return;
		synchronized (currentMethod) {
			currentMethod = method;
		}
	}
	
	/**
	 * Get the connect method using the current connect method ({@link ConnectMethod#changeConnectMethod(ConnectMethods)}
	 * @param clientSocket the clientSocket
	 * @return the current default connect method
	 */
	public static final ConnectMethod getConnectMethod(Socket clientSocket, Socks5Proxy proxy) {
		synchronized (currentMethod) {
			switch (currentMethod) {
			case TCP:
				return new ConnectMethodTCP(clientSocket, proxy);

			default:
				return null;
			}
		}
	}
	
}
