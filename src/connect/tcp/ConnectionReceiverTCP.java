package connect.tcp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;

import proxy.Socks5Proxy;

public class ConnectionReceiverTCP extends ConnectionReceiver {
	private static final int TIMEOUT = 1000 * 10;
	private static final int ARRAYSIZE = 1024;
	private Socket from;
	private Socket to;
	
	private final Socks5Proxy proxy;
	
	public ConnectionReceiverTCP(Socket from, Socket to, Socks5Proxy proxy) {
		this.from = from;
		this.to = to;
		this.proxy = proxy;
		try {
			from.setSoTimeout(TIMEOUT);
			to.setSoTimeout(TIMEOUT);
		} catch (SocketException e) {
		}
	}
	
	@Override
	public String toString() {
		return from.toString() + " -> " + to.toString();
	}
	
	@Override
	public void run() {
		try {
			final InputStream i = from.getInputStream();
			final OutputStream o = to.getOutputStream();
			int read = 0;
			byte[] bytes = new byte[ARRAYSIZE];
			while (!to.isOutputShutdown() && this.proxy.isRunning()) {
				try {
					read = i.read(bytes);
				} catch (SocketTimeoutException e) {
					continue;
				}
				if (read < 0) {
					to.shutdownOutput();
				} else {
					o.write(bytes, 0, read);
					o.flush();
				}
			}
		} catch (Exception e) {
			try { from.close();} catch (IOException e1) {}
			try { to.close(); } catch (IOException e1) {}
		}
	}
	
	public static void start(Socket clientSocket, Socket out, Socks5Proxy proxy) {
		ThreadGroup tg = new ThreadGroup(clientSocket.getInetAddress() + ":" + clientSocket.getPort() + " - " + out.getInetAddress() + ":" + out.getPort());
		tg.setDaemon(true);
		Thread t;
		t = new Thread(tg, new ConnectionReceiverTCP(clientSocket, out, proxy), clientSocket.getInetAddress() + ":" + clientSocket.getPort() + " -> " + out.getInetAddress() + ":" + out.getPort());
		t.setDaemon(true);
		t.start();
		
		t = new Thread(tg, new ConnectionReceiverTCP(out, clientSocket, proxy), clientSocket.getInetAddress() + ":" + clientSocket.getPort() + " <- " + out.getInetAddress() + ":" + out.getPort());
		t.setDaemon(true);
		t.start();
	}
}
