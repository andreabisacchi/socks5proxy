package requests.authentication;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ClientAuthenticationRequest {
	private static final char NOAUTHENTICATION = 0;
	private static final char NOMETHODS = 255;
	private static final VersionNotSupportedException exception = new VersionNotSupportedException("Version not supported, only version 5.");
	private char version;
	private char nmethods;
	private char[] methods;
	
	public char getVersion() {
		return version;
	}

	public char getNmethods() {
		return nmethods;
	}

	public char[] getMethods() {
		return methods;
	}

	public void writeResponse(OutputStream out) throws IOException {
		boolean isValid = false;
		for (char method : this.getMethods()) {
			if (method == NOAUTHENTICATION)
				isValid = true;
		}
		final char toWrite = (isValid ? NOAUTHENTICATION : NOMETHODS);
		byte[] reply = {(byte) this.getVersion(), (byte) toWrite};
		out.write(reply);
		out.flush();
	}
	
	@Override
	public String toString() {
		return "Authentication request. Version=" + this.version + ", NMethods=" + this.nmethods + ",Methods=" + this.methods.toString();
	}
	
	public static ClientAuthenticationRequest parse(InputStream in) throws IOException, IllegalStateException, VersionNotSupportedException {
		ClientAuthenticationRequest result = new ClientAuthenticationRequest();
		
		result.version = read(in);
		result.nmethods = read(in);
		result.methods = new char[result.nmethods];
		for (int i = 0; i < result.nmethods; i++) {
			result.methods[i] = read(in);
		}
		if (result.version != 5) {
			System.err.println("Not supported version: " + (int)result.version);
			throw exception;
		}
		
		boolean accept = false;
		for (char method : result.methods) {
			if (method == NOAUTHENTICATION) // Accepted only NOATHENTICATION
				accept = true;
		}
		if (!accept)
			throw new IllegalStateException();
		
		return result;
	}
	
	private static char read(InputStream in) throws IOException {
		int read = in.read();
		if (read < 0) throw new IOException();
		else return (char)read;
	}
	
}
