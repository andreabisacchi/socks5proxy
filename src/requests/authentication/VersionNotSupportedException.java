package requests.authentication;

public class VersionNotSupportedException extends Exception {

	private static final long serialVersionUID = 7644920426464742819L;

	public VersionNotSupportedException() {
	}

	public VersionNotSupportedException(String arg0) {
		super(arg0);
	}

	public VersionNotSupportedException(Throwable arg0) {
		super(arg0);
	}

	public VersionNotSupportedException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public VersionNotSupportedException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

}
