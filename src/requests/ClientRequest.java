package requests;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;

import proxy.Socks5Proxy;
import requests.addresses.AddressTypes;
import requests.addresses.RequestAddress;

public abstract class ClientRequest {
	private char version;
	private char rsv;
	private int port;
	private RequestAddress address;
	private Socket socket;

	private AtomicBoolean replySent = new AtomicBoolean(false);
	private Semaphore sem = new Semaphore(0);

	protected ClientRequest(Socket s, char version, char rsv, RequestAddress address, int port) {
		if (rsv != 0)
			throw new IllegalArgumentException();

		this.version = version;
		this.rsv = rsv;
		this.address = address;
		this.port = port;
		this.socket = s;
	}

	protected Socket getSocket() {
		return this.socket;
	}

	public char getVersion() {
		return version;
	}

	public abstract char getCommand();

	public char getReserved() {
		return rsv;
	}

	public char getAddressTypeChar() {
		return this.address.getAddressTypeChar();
	}

	public AddressTypes getAddressType() {
		return this.address.getAddressType();
	}

	public int getPort() {
		return port;
	}

	public RequestAddress getRequestAddress() {
		return this.address;
	}
	
	@Override
	public String toString() {
		return this.address.toString() + ":" + this.port;
	}

	public abstract void executeAndWriteResponse(OutputStream out, Socks5Proxy socks5Proxy) throws IOException;

	public static ClientRequest parse(Socket socket) throws IOException {
		InputStream in = socket.getInputStream();
		ClientRequest result = null;
		char version = read(in);
		char cmd = read(in);
		RequestCommands command = RequestCommands.of(cmd);
		char rsv = read(in);
		char atyp = read(in);
		AddressTypes addressType = AddressTypes.of(atyp);
		RequestAddress address = addressType.getRequestAddress(in);
		int port = new DataInputStream(in).readUnsignedShort();

		switch (command) {
		case CONNECT:
			result = new ClientRequestConnect(socket, version, rsv, address, port);
			break;

		default:
			break;
		}

		return result;
	}

	private static char read(InputStream in) throws IOException {
		int read = in.read();
		if (read < 0) 
			throw new IOException();
		else return (char)read;
	}

	public final void waitForSending() {
		if (!this.replySent.get()) {
			try {
				this.sem.acquire();
			} catch (InterruptedException e) {
			}
		}
	}

	protected final void replySent() {
		if (!this.replySent.get()) {
			this.replySent.set(true);
			this.sem.release();
		}
	}

}
