package requests;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.Arrays;

import connect.ConnectThread;
import proxy.Socks5Proxy;
import requests.addresses.AddressTypes;
import requests.addresses.RequestAddress;
import requests.addresses.RequestDomainAddress;

public class ClientRequestConnect extends ClientRequest {
	private static final char CHARCONNECT = 1;
	private static final char CHARCOMMANDNOTSUPPORTED = 7;

	protected ClientRequestConnect(Socket s, char version, char rsv, RequestAddress address, int port) {
		super(s, version, rsv, address, port);
	}

	@Override
	public char getCommand() {
		return CHARCONNECT;
	}
	
	@Override
	public String toString() {
		return "CONNECT to " + super.toString();
	}

	@Override
	public void executeAndWriteResponse(OutputStream out, Socks5Proxy socks5Proxy) throws IOException {
		boolean ipv4 = false;
		switch (this.getAddressType()) {
		case DOMAINNAME:
			RequestDomainAddress domainAddress = (RequestDomainAddress) this.getRequestAddress();
			String host = domainAddress.getAddress();
			if (InetAddress.getByName(host) instanceof Inet4Address)
				ipv4 = true;
			break;
		case IPV4:
			ipv4 = true;
			break;
		case IPV6:
			ipv4 = false;
			break;
		default:
			break;
		}

		final ByteBuffer byteBuffer = ByteBuffer.allocate(256 + 6);
		if (ipv4) {
			ConnectThread connectThread = new ConnectThread(this, this.getSocket(), socks5Proxy);
			connectThread.setDaemon(true);
			connectThread.start();
			byteBuffer.put((byte)this.getVersion());
			byteBuffer.put((byte)connectThread.getReply());
			byteBuffer.put((byte)this.getReserved());
			AddressTypes addressType = connectThread.getAddressType();
			byteBuffer.put((byte)addressType.getAddressType());
			switch (addressType) {
			case IPV4:
				byte[] address = connectThread.getAddress().getAddress();
				byteBuffer.put(address);
				break;

			default:
				break;
			}
			byteBuffer.putShort((short) connectThread.getPort());
			out.write(byteBuffer.array(), 0, byteBuffer.position());
			out.flush();
			this.replySent();
		} else { // IPv6
			byteBuffer.put((byte)this.getVersion());
			byteBuffer.put((byte)CHARCOMMANDNOTSUPPORTED);
			byteBuffer.put((byte)this.getReserved());
			byteBuffer.put((byte)AddressTypes.IPV6.getAddressType());
			byte[] addressAndPort = new byte[16+2];
			Arrays.fill(addressAndPort, (byte)0);
			byteBuffer.put(addressAndPort);
			out.write(byteBuffer.array(), 0, byteBuffer.position());
			out.flush();
		}
	}

}
