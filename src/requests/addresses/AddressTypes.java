package requests.addresses;

import java.io.IOException;
import java.io.InputStream;

public enum AddressTypes {
IPV4(AddressTypes.CHARIPV4), DOMAINNAME(AddressTypes.CHARDOMAINNAME), IPV6(AddressTypes.CHARIPV6);
	
	private static final char CHARIPV4 = 1;
	private static final char CHARDOMAINNAME = 3;
	private static final char CHARIPV6 = 4;
	
	private char atyp;
	AddressTypes(char atyp) {
		this.atyp = atyp;
	}
	
	public char getAddressType() {
		return atyp;
	}
	
	public static AddressTypes of(char atyp) {
		for (AddressTypes current : AddressTypes.values()) {
			if (current.getAddressType() == atyp)
				return current;
		}
		return null;
	}

	public RequestAddress getRequestAddress(InputStream in) throws IOException {
		switch (this) {
		case DOMAINNAME:
			return RequestDomainAddress.parse(in);
			
		case IPV4:
			return RequestIPv4Address.parse(in);

		default:
			break;
		}
		return null;
	}
	
}
