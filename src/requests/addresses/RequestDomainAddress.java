package requests.addresses;

import java.io.IOException;
import java.io.InputStream;

public class RequestDomainAddress extends RequestAddress {
	private static final char ADDRESSTYPE = 3; 
	
	private String address;
	
	private RequestDomainAddress(String address) {
		this.address = address;
	}

	public String getAddress() {
		return this.address;
	}
	
	@Override
	public char getAddressTypeChar() {
		return ADDRESSTYPE;
	}
	
	@Override
	public AddressTypes getAddressType() {
		return AddressTypes.DOMAINNAME;
	}
	
	@Override
	public String toString() {
		return this.address;
	}
	
	public static RequestDomainAddress parse(InputStream in) throws IOException {
		int lenght = in.read();
		char[] chars = new char[lenght];
		for(int i = 0; i < lenght; i++)
			chars[i] = read(in);
		return new RequestDomainAddress(new String(chars));
	}
	
	private static char read(InputStream in) throws IOException {
		int read = in.read();
		if (read < 0) throw new IOException();
		else return (char)read;
	}
	
}
