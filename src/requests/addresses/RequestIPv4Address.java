package requests.addresses;

import java.io.IOException;
import java.io.InputStream;
import java.net.Inet4Address;
import java.net.InetAddress;

public class RequestIPv4Address extends RequestAddress {
private static final char ADDRESSTYPE = 1;
	
	private InetAddress address;
	
	private RequestIPv4Address(InetAddress inetAddress) {
		this.address = inetAddress;
	}

	public InetAddress getAddress() {
		return this.address;
	}
	
	@Override
	public char getAddressTypeChar() {
		return ADDRESSTYPE;
	}
	
	@Override
	public AddressTypes getAddressType() {
		return AddressTypes.IPV4;
	}
	
	@Override
	public String toString() {
		return this.address.toString();
	}
	
	public static RequestIPv4Address parse(InputStream in) throws IOException {
		byte[] bytes = new byte[4];
		for (int i = 0; i < 4; i++)
			bytes[i] = read(in);
		return new RequestIPv4Address(Inet4Address.getByAddress(bytes));
	}
	
	private static byte read(InputStream in) throws IOException {
		int read = in.read();
		if (read < 0) throw new IOException();
		else return (byte)read;
	}

}
