package requests.addresses;

public abstract class RequestAddress {

	public abstract char getAddressTypeChar();
	
	public abstract AddressTypes getAddressType();
	
	public abstract String toString();
	
}
