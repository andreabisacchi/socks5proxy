package requests;

public enum RequestCommands {
CONNECT(RequestCommands.CHARCONNECT), BIND(RequestCommands.CHARBIND), UDP(RequestCommands.CHARUDP);
	
	private static final char CHARCONNECT = 1;
	private static final char CHARBIND = 2;
	private static final char CHARUDP = 3;
	
	private char cmd;
	RequestCommands(char cmd) {
		this.cmd = cmd;
	}
	
	public char getCommand() {
		return cmd;
	}
	
	public static RequestCommands of(char cmd) {
		for (RequestCommands current : RequestCommands.values()) {
			if (current.getCommand() == cmd)
				return current;
		}
		return null;
	}
	
}
