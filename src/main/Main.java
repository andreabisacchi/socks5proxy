package main;

import java.io.IOException;

import connect.blacklist.Blacklist;
import connect.methods.ConnectMethod;
import connect.methods.ConnectMethods;
import proxy.Socks5Proxy;

public class Main {

	public static void main(String[] args) {
		ConnectMethod.changeConnectMethod(ConnectMethods.TCP);
		final Socks5Proxy proxy;
		if (args.length > 0)
			proxy = new Socks5Proxy(Integer.parseInt(args[0]));
		else
			proxy = new Socks5Proxy();
		proxy.getBlacklist().addPredicate(p -> !(p.getElement0().isAnyLocalAddress() || p.getElement0().isLoopbackAddress() || p.getElement0().isMulticastAddress()));
		proxy.getBlacklist().addPredicate(Blacklist.getPredicatePrivateNetworks());
		
		proxy.start();
		
		System.out.println("Press any key to stop...");
		try {
			System.in.read();
		} catch (IOException e) {}
		proxy.stop();
	}

}
